FROM python:3.6

ENV PYTHONUNBUFFERED=1
COPY ./requirements.txt ./app/requirements.txt
WORKDIR ./app
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . .
